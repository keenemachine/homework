package asolovyov.DZ8.Dictionary;

import java.util.HashMap;
import java.util.Map;

public class Translator {

    private final Map<String, String> dictionary;

    public Translator() {
        dictionary = new HashMap<>();
    }

    public void addWord(String englishWord, String russianTranslation) {
        this.dictionary.put(englishWord, russianTranslation);
    }

    public String translation(String englishWord) {
        return this.dictionary.getOrDefault(englishWord, "undefined");
    }

    public void removeWord(String englishWord) {
        this.dictionary.remove(englishWord);
    }
}
