package asolovyov.DZ8.Dictionary;

public class MainTranslator {
    public static void main(String[] args) {
        Translator translator = new Translator();
        translator.addWord("Hello", "Привет");
        translator.addWord("Say", "Говорить");
        translator.addWord("My", "Мой");
        translator.addWord("Name", "Имя");
        translator.addWord("Heisenberg", "Гизенбург");

        System.out.println(translator.translation("Say") +
                " " + translator.translation("My") + " " +
                translator.translation("Name"));
        
        translator.removeWord("Heisenberg");
    }
}
