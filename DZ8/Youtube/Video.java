package asolovyov.DZ8.Youtube;

import java.util.Objects;

public class Video implements Comparable<Video>{
    String author;
    String name;
    int duration;
    String comments;

    public Video(String author, String name, int duration, String comments) {
        this.author = author;
        this.name = name;
        this.duration = duration;
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Видео: " +
                "автор: " + author +
                ", наименование: " + name +
                ", длительность: " + duration +
                ", комментарии: " + comments;
    }

    @Override
    public int compareTo(Video another) {
        return Integer.compare(this.duration, another.duration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Video video = (Video) o;
        return duration == video.duration && Objects.equals(name, video.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, duration);
    }
}
