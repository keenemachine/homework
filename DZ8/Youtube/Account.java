package asolovyov.DZ8.Youtube;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Account {

    ArrayList<Video> videos = new ArrayList<>();

    @Override
    public String toString() {
        return "Account{" +
                "videos=" + videos +
                '}';
    }

    public void displayAllVideos(ArrayList<Video> videos) {
        System.out.println("Список понравившихся видео:");
        for (Video video : videos) {
            System.out.println(video);
        }
    }

    public void displayAllVideoByDurationAscComparable(ArrayList<Video> videos) {
        System.out.println("Список понравившихся видео по возрастанию длительности:");
        Collections.sort(videos,(Video o1, Video o2) -> { return o1.compareTo(o2);} );
        for (Video video : videos) {
            System.out.println(video);
        }
    }

    public void displayAllVideoByDurationAscComparator(ArrayList<Video> videos) {
        System.out.println("Список понравившихся видео по возрастанию длительности:");
        videos.sort(new Comparator<Video>() {
            @Override
            public int compare(Video o1, Video o2) {
                return o1.duration - o2.duration;
            }
        });
        for (Video video : videos) {
            System.out.println(video);
        }
    }

    public void addVideo(Video video) {
        videos.add(video);
    }

    public void removeVideo(Video video) {
        videos.remove(video);
    }

    public void removeDublicates() {
        ArrayList<Video> uniqueVideos = new ArrayList<>(videos);
        uniqueVideos.removeIf(video -> !uniqueVideos.contains(video));
        videos = uniqueVideos;
    }
}
