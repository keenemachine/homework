package asolovyov.DZ8.Youtube;

import java.util.ArrayList;

public class MainYoutube {
    public static void main(String[] args) {
        Account account = new Account();
        ArrayList<Video> videos = new ArrayList<>();
        videos.add(new Video("Vasiliy1337", "Memamomi", 10, "жесть"));
        videos.add(new Video("S. Baskova", "Green Elephant", 68, "Курлык!"));
        videos.add(new Video("Psy", "GAHFMGHNAM STYLE", 4, "Чётко! Умеете, могёте."));
        videos.add(new Video("Сергей Пахомов", "Green Elephant", 68, "Я смотрел это видео полностью, в конце он кричал 'Мама, я не хочу в яму'"));
        account.displayAllVideos(videos);
        account.displayAllVideoByDurationAscComparable(videos);
        account.displayAllVideoByDurationAscComparator(videos);
        account.addVideo(new Video("", "", 3, ""));
        account.removeVideo(videos.get(3));
        account.removeDublicates();
    }
}
