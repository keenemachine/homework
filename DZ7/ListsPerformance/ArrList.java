package asolovyov.Performance;

import java.util.ArrayList;
import java.util.Random;

public class ArrList {

    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 1000000; i++) {
            arrList.add(random.nextInt());
        }
        long timeBeforeAdd = System.currentTimeMillis();
        adding1(arrList);
        long timeAfterAdd = System.currentTimeMillis();
        long timeBeforeShowing = System.currentTimeMillis();
        showing100(arrList);
        long timeAfterShowing = System.currentTimeMillis();
        long timeBeforeDeleting = System.currentTimeMillis();
        deleting1000(arrList);
        long timeAfterDeleting = System.currentTimeMillis();
        long timeBeforeSetting = System.currentTimeMillis();
        setting(arrList);
        long timeAfterSetting = System.currentTimeMillis();
        System.out.printf("Время выполнения операции добавления: %d миллисекунд\n", (timeAfterAdd-timeBeforeAdd));
        System.out.printf("Время выполнения операции отображения: %d миллисекунд\n", (timeAfterShowing-timeBeforeShowing));
        System.out.printf("Время выполнения операции удаления: %d миллисекунд\n", (timeAfterDeleting-timeBeforeDeleting));
        System.out.printf("Время выполнения операции изменения: %d миллисекунд", (timeAfterSetting-timeBeforeSetting));
    }

    public static void adding1(ArrayList<Integer> arrList) {
        for (int i = 0; i < 1000; i++) {
            arrList.add(i, i++);
        }
    }

    public static void showing100(ArrayList<Integer> arrList) {
        for (int i = 500000; i <= 500100; i++) {
            System.out.println(i);
        }
    }

    public static void deleting1000(ArrayList<Integer> arrList) {
        for (int i = 0; i <= 1000; i++) {
            arrList.remove(i);
        }
    }

    public static void setting(ArrayList<Integer> arrList) {
        Random random = new Random();
        for (int i = 500000; i < 500100; i++) {
            arrList.set(i, random.nextInt()+2);
        }
    }
}

/**
 * Результаты
 * Время выполнения операции добавления: 328 миллисекунд
 * Время выполнения операции отображения: 0 миллисекунд
 * Время выполнения операции удаления: 485 миллисекунд
 * Время выполнения операции изменения: 0 миллисекунд
 */