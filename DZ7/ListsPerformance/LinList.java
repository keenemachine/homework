package asolovyov.Performance;

import java.util.LinkedList;
import java.util.Random;

public class LinList {

    public static void main(String[] args) {
        LinkedList<Integer> linList = new LinkedList<>();
        Random random = new Random();
        for (int i = 0; i < 1000000; i++) {
            linList.add(random.nextInt());
        }
        long timeBeforeAdd = System.currentTimeMillis();
        adding1(linList);
        long timeAfterAdd = System.currentTimeMillis();
        long timeBeforeShowing = System.currentTimeMillis();
        showing100(linList);
        long timeAfterShowing = System.currentTimeMillis();
        long timeBeforeDeleting = System.currentTimeMillis();
        deleting1000(linList);
        long timeAfterDeleting = System.currentTimeMillis();
        long timeBeforeSetting = System.currentTimeMillis();
        setNewValues(linList);
        long timeAfterSetting = System.currentTimeMillis();
        System.out.printf("Время выполнения операции добавления: %d миллисекунд\n", (timeAfterAdd-timeBeforeAdd));
        System.out.printf("Время выполнения операции отображения: %d миллисекунд\n", (timeAfterShowing-timeBeforeShowing));
        System.out.printf("Время выполнения операции удаления: %d миллисекунд\n", (timeAfterDeleting-timeBeforeDeleting));
        System.out.printf("Время выполнения операции изменения: %d миллисекунд", (timeAfterSetting-timeBeforeSetting));
    }

    public static void adding1(LinkedList<Integer> linList) {
        for (int i = 0; i < 1000; i++) {
            linList.add(i, i++);
        }
    }

    public static void showing100(LinkedList<Integer> linList) {
        for (int i = 500000; i <= 500100; i++) {
            System.out.println(i);
        }
    }

    public static void deleting1000(LinkedList<Integer> linList) {
        for (int i = 0; i <= 1000; i++) {
            linList.remove(i);
        }
    }

    public static void setNewValues(LinkedList<Integer> linList) {
        Random random = new Random();
        for (int i = 500000; i < 500100; i++) {
            linList.set(i, random.nextInt()+2);
        }
    }
}

/**
 * Результаты
 * Время выполнения операции добавления: 16 миллисекунд
 * Время выполнения операции отображения: 0 миллисекунд
 * Время выполнения операции удаления: 15 миллисекунд
 * Время выполнения операции изменения: 456 миллисекунд
 */