package asolovyov.ArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class Colors {
    public static void main(String[] args) {
        String[] Colors = {"красный", "синий", "черный", "терракотовый", "фиолетовый", "малиновый", "серый"};
        ArrayList<String> colorsArray = new ArrayList<>(Arrays.asList(Colors));
        Iterator<String> iterator = colorsArray.iterator();
        while (iterator.hasNext()) {
            String color = iterator.next();
            System.out.println("Вывод из итератора: " + color);
        }
        Collections.sort(colorsArray);
        colorsArray.set(1, "Dark " + colorsArray.get(1));
        colorsArray.set(3, "Dark " + colorsArray.get(3));
        colorsArray.set(5, "Dark " + colorsArray.get(5));
        for (String color : colorsArray) {
            System.out.println("Вывод c Dark: " + color);
        }
        for (int i = 0; i < 5; i++) {
            String color = colorsArray.get(i);
            System.out.println((i+1) + " цвет в подсписке: " + color);
        }
        Collections.swap(colorsArray, 1,4);
        String a1 = colorsArray.get(3);
        boolean hasA1 = colorsArray.contains(a1);
        System.out.println(hasA1);
        while (iterator.hasNext()) {
            String color = iterator.next();
            if (color.contains("o")) {
                iterator.remove();
            }
        }
        Object[] arrayOfColor = colorsArray.toArray();
        for (Object color : arrayOfColor) {
            System.out.println(color);
        }
        System.out.println("Увы, чаем с печенькой здесь не обойтись, пойду за чем-нибудь покрепче");
    }
}
