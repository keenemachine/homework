package asolovyov.HashEquals;

import java.util.ArrayList;
import java.util.Arrays;

public class MainHash {

    public static void main(String[] args) {
        VacuumCleaner vc1 = new VacuumCleaner(8800, "Super", "Xuemo", 'C');
        VacuumCleaner vc2 = new VacuumCleaner(8800, "Super", "Xuemo", 'C');
        VacuumCleaner vc3 = new VacuumCleaner(28000, "Bazooka", "Whereher", 'A');
        System.out.println(vc1.equals(vc2));
        System.out.println(vc1.equals(vc3));
        System.out.println(vc2.equals(vc3));
        ArrayList<VacuumCleaner> list = new ArrayList<>(Arrays.asList(vc1, vc2, vc3));
        for (Object v : list) {
            System.out.println("Пылесосхэш: " + v.hashCode());
        }
    }
}
