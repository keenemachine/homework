package asolovyov.HashEquals;

public class VacuumCleaner {
    public static String brand = "LG";
    public int price;
    public static char energyClass = 'A';
    public String model;

    public VacuumCleaner(int price, String model, String brand, char energyClass) {
          this.price = price;
          this.model = model;
          VacuumCleaner.brand = brand;
          VacuumCleaner.energyClass = energyClass;
     }

     @Override
     public boolean equals(Object o) {
          if (!(o instanceof VacuumCleaner other)) {
               return false;
          }
         return this.model.equals(other.model) && this.price == other.price;
     }

     @Override
     public int hashCode() {
          return this.model.length() + this.price*146 + brand.toCharArray().length + (int) energyClass;
     }
}
