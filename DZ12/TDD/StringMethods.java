package asolovyov.DZ12.TDD;

import static org.apache.commons.lang3.StringUtils.reverse;

public class StringMethods {

    public String bracketDecorate(String input) {
        String[] words = input.trim().split(" ");
        StringBuilder bracketedWords = new StringBuilder();
        for (String w : words) {
            bracketedWords.append('[').append(w).append(']');
        }
        return bracketedWords.toString();
    };

    public String reverseString(String input) {
        if (input.length() == 0) {
            return null;
        } else {
            return reverse(input);
        }
    }
}
