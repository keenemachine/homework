package asolovyov.DZ12.TDD;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.StringUtils.reverse;

public class StringTestsRegress {

    StringMethods stringMethods = new StringMethods();
    String inputString;
    String expectedString;
    String bracketString;

    @Test
    public void bracketTest1() {
        inputString = "Ba BA bu";
        expectedString = "[Ba][BA][bu]";
        bracketString = stringMethods.bracketDecorate(inputString);
        Assert.assertEquals(expectedString, bracketString);
    }

    @Test
    public void bracketTest2() {
        inputString = "";
        expectedString = "[]";
        bracketString = stringMethods.bracketDecorate(inputString);
        Assert.assertEquals(expectedString, bracketString);
    }

    @Test
    public void bracketTest3() {
        inputString = "0,12123123";
        expectedString = '[' + inputString + ']';
        bracketString = stringMethods.bracketDecorate(inputString);
        Assert.assertEquals(expectedString, bracketString);
    }

    @Test
    public void reverseTest1() {
        inputString = "12345abab";
        expectedString = reverse(inputString);
        Assert.assertEquals(expectedString, stringMethods.reverseString(inputString));
    }

    @Test
    public void reverseTest2() {
        inputString = "";
        Assert.assertNull(stringMethods.reverseString(inputString));
    }

    @Test
    public void reverseTest3() {
        inputString = "BARABABABABA AS DASA AS DA D ASD  ASD ASD !";
        expectedString = reverse(inputString);
        Assert.assertEquals(expectedString, stringMethods.reverseString(inputString));
    }
}
