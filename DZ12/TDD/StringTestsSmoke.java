package asolovyov.DZ12.TDD;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.StringUtils.reverse;

public class StringTestsSmoke {

    StringMethods stringMethods = new StringMethods();
    String inputString;
    String expectedString;
    String bracketString;

    @Test
    public void bracketTest1() {
        inputString = "Ba BA bu";
        expectedString = "[Ba][BA][bu]";
        bracketString = stringMethods.bracketDecorate(inputString);
        Assert.assertEquals(expectedString, bracketString);
    }

    @Test
    public void reverseTest1() {
        inputString = "12345abab";
        expectedString = reverse(inputString);
        Assert.assertEquals(expectedString, stringMethods.reverseString(inputString));
    }
}