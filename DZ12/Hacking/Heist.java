package asolovyov.DZ12.Hacking;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class Heist extends Vault {

    public static void main(String[] args) {

        Vault federalVault = new Vault();
        System.out.println("До взлома " + federalVault);

        Vault snatchVault = new Vault();
        int myDollars;
        int myEuros;
        double myTonsOfGold;
        String myPentagonNukesCodes;

        try {
            Field dollars = federalVault.getClass().getDeclaredField("dollars");
            dollars.setAccessible(true);
            myDollars = (int) dollars.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        try {
            Field euros = federalVault.getClass().getDeclaredField("euros");
            euros.setAccessible(true);
            myEuros = (int) euros.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        try {
            Field tonsOfGold = federalVault.getClass().getDeclaredField("tonsOfGold");
            tonsOfGold.setAccessible(true);
            myTonsOfGold = (double) tonsOfGold.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        try {
            Field pentagonNukesCodes = federalVault.getClass().getDeclaredField("pentagonNukesCodes");
            pentagonNukesCodes.setAccessible(true);
            myPentagonNukesCodes = (String) pentagonNukesCodes.get(federalVault);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        Field[] fields = federalVault.getClass().getDeclaredFields();
        Arrays.stream(fields).forEach(field -> {
            try {
                field.setAccessible(true);
                field.set(federalVault, 0);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Arrays.stream(fields).forEach(field -> {
            try {
                field.setAccessible(true);
                field.set(federalVault, null);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        System.out.println("Это что осталось после взлома " + federalVault);

        try {
            Constructor<?> myVault = federalVault.getClass().getDeclaredConstructor(int.class, int.class, double.class, String.class);
            myVault.setAccessible(true);
            snatchVault = (Vault) myVault.newInstance(myDollars, myEuros, myTonsOfGold, myPentagonNukesCodes);
            System.out.println("Это теперь моё " + snatchVault);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
