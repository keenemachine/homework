package asolovyov.DZ11;

import java.util.*;

public class StringUtils {

    public boolean validateEmail (String email) throws IncorrectEmailException {
        if (email.matches("([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.[a-zA-Z0-9_-]+)")) {
            System.out.println("Проверено!");
            return true;
        } else {
            throw new IncorrectEmailException();
        }
    }

    public boolean isEven (Integer number) {
        if(number % 2 == 0){
            return true;
        } else {
            return false;
        }
    }

    public List<String> filterList (List<String> strings) {
        List<String> result = new ArrayList<>();
        for (String string : strings) {
            if (string.length() <= 6 || string.startsWith("a")) {
                result.add(string);
            }
        }
        return result;
    }

}