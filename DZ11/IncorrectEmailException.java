package asolovyov.DZ11;

public class IncorrectEmailException extends Throwable
{
    public IncorrectEmailException()
    {
        super("Invalid email address.");
    }
}
