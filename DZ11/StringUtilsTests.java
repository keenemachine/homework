package asolovyov.DZ11;

import org.junit.*;
import org.testng.annotations.*;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.List;

public class StringUtilsTests {

    StringUtils stringUtils = new StringUtils();

    @Test
    public void emailTest1() throws IncorrectEmailException {
        Assert.assertTrue(stringUtils.validateEmail("pepe@kek.com"));
    }

    @Test
    public void emailTest2() {
        Assert.assertFalse("Invalid email address.", false);
    }

    @Test(dataProvider = "intList")
    public void isEvenTest1(Integer number) {
        Assert.assertTrue(stringUtils.isEven(number));
    }

    @Test
    public void isEvenTest2() {
        Assert.assertFalse(stringUtils.isEven(3));
    }

    @DataProvider(name = "intList")
    public static Object[][] intList() {
        return new Object[][]{
                {2},
                {4},
                {6},
        };
    }

    @Test
    public void filterListTest1() {
        List<String> input = Arrays.asList("hello", "abcd");
        List<String> output = stringUtils.filterList(input);
        Assert.assertEquals(output, Arrays.asList("hello", "abcd"));
    }

    @Test
    public void filterListTest2() {
        List<String> input = Arrays.asList("123123", "123123");
        List<String> output = stringUtils.filterList(input);
        Assert.assertEquals(output, Arrays.asList("123123", "123123"));
    }
}
