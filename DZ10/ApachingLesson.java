package asolovyov.DZ10;

import static org.apache.commons.lang3.StringUtils.reverse;
import static org.apache.commons.lang3.StringUtils.swapCase;

public class ApachingLesson {
    public static void main(String[] args) {
        String reverse = "ZDAROVA?";
        String register = "Привет";

        System.out.println(reverse(reverse));
        System.out.println(swapCase(register));
    }
}
