package asolovyov.DZ9.CosmicProgramm;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MainCandidateSelector {
    public static void main(String[] args) {
        List<Human> humans = new ArrayList<>();
        humans.add(new Human(1,"Ivan", 33));
        humans.add(new Human(2,"Vasiliy", 42));
        humans.add(new Human(3, "Marina", 21));
        humans.add(new Human(4, "Katerina", 29));
        humans.add(new Human(5, "Ibrahim", 26));
        humans.add(new Human(6, "Ihsahn", 48));
        humans.add(new Human(7, "Chingachkuk", 30));
        humans.add(new Human(8, "Bayoyajo", 40));
        humans.add(new Human(9, "Kurt", 27));
        humans.add(new Human(10, "Chelsey", 38));

        List<Human> candidates = humans.stream()
                .limit(8)
                .sorted((h1, h2) -> h2.age - h1.age)
                .filter(human -> human.age >= 20)
                .filter(human -> human.age <= 45).toList();

        for (Human human : candidates) {
            System.out.println(human);
        }

        System.out.println("Поздравляем счастливчиков, прошедших отборочные испытания! \n" +
                "Теперь сделаем из вас настоящих космонавтов!");

        candidates.stream()
                .map(h -> new Cosmonaut(h.name, h.age, "Земля"))
                .sorted(Comparator.comparing(c -> c.name)).toList()
                .forEach(System.out::println);
    }
}
