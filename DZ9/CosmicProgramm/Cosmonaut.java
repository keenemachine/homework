package asolovyov.DZ9.CosmicProgramm;

public class Cosmonaut {
    String name;
    int age;
    String departurePlanet;

    public Cosmonaut(String name, int age, String departurePlanet) {
        this.name = name;
        this.age = age;
        this.departurePlanet = departurePlanet;
    }

    @Override
    public String toString() {
        return "Космонавт [" +
                "Имя: " + name +
                ", возраст: " + age +
                ", планета отправки: " + departurePlanet +
                ']';
    }
}
