package asolovyov.DZ9.CosmicProgramm;

public class Human {
    int id;
    String name;
    int age;

    public Human(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Данные кандидата [" +
                "id: " + id +
                ", Имя: " + name +
                ", возраст: " + age +
                ']';
    }
}
