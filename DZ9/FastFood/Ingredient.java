package asolovyov.DZ9.Fastfood;

public class Ingredient {
    String name;

    Ingredient(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    boolean isForBurger() {
        return !name.equals("нижняя булочка");
    }
}
