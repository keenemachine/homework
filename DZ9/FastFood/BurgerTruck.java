package asolovyov.DZ9.Fastfood;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class BurgerTruck {

    public static List<Ingredient> Ingredients = Arrays.asList(
            new Ingredient("огурцы"),
            new Ingredient("помидоры"),
            new Ingredient("котлета"),
            new Ingredient("сыр"),
            new Ingredient("кетчуп"),
            new Ingredient("майонез"),
            new Ingredient("булочка"),
            new Ingredient("салат"));

    public static void main(String[] args) {
        Random rand = new Random();

        for (int i = 0; i < 8; i++) {
            List<Ingredient> pack = Stream.generate(() -> {
                        return Ingredients.get(rand.nextInt(8));
                    })
                    .limit(5)
                    .collect(Collectors.toList());
            if (hasIngredientsForBurger(pack)) {
                makeBurger(pack);
            } else {
                System.out.println("Недостаточно ингредиентов для бургера.");
            }
        }
    }

    public static boolean hasIngredientsForBurger(List<Ingredient> ingredients) {
        return ingredients.stream().filter(Ingredient::isForBurger).count() == 3;
    }

    public static void makeBurger(List<Ingredient> ingredients) {
        List<Ingredient> sortedIngredients = sortIngredients(ingredients);
        System.out.printf("Готовый бургер: %s%n", sortedIngredients.stream().map(Ingredient::getName).collect(joining(", ")));
    }

    public static List<Ingredient> sortIngredients(List<Ingredient> ingredients) {
        if (ingredients.stream().anyMatch(Ingredient::isForBurger)) {
            ingredients.add(0, new Ingredient("нижняя булочка"));
        } else {
            System.out.println("Необходимо добавить булочку.");
        }
        ingredients.sort(Comparator.comparing(Ingredient::getName));
        Collections.reverse(ingredients);
        return ingredients;
    }
}
