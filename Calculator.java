package asolovyov.ConsoleCalculator;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        System.out.println("Инструкция. Введите математическое выражение для вычисления в форматах: \n" +
                "1) число%пробел%символ_оператора_математической_операции%пробел%число \n" +
                "2) число%пробел%оператор_в_текстовом_виде%пробел%число \n" +
                "_________________________________________________________");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine().toLowerCase();
        String[] parts = input.split(" ");

        double num1 = Double.parseDouble(parts[0]);
        double num2 = Double.parseDouble(parts[2]);
        String operation = parts[1];

        try {
            switch (operation) {
                case "+":
                case "плюс":
                    System.out.printf("Результат: %f%n", num1 + num2);
                    break;
                case "-":
                case "минус":
                    System.out.printf("Результат: %f%n", num1 - num2);
                    break;
                case "*":
                case "умножить":
                    System.out.printf("Результат: %f%n", num1 * num2);
                    break;
                case "/":
                case "делить":
                    if (num2 != 0) {
                        System.out.printf("Результат: %f%n", num1 / num2);
                    } else {
                        System.err.println("Ошибка: деление на ноль!");
                    }
                    break;
            }

        } catch (Exception e) {
            System.err.println("Ошибка: Неверный формат ввода, изучите инструкцию и попробуйте снова \n" + e.getMessage());
            e.printStackTrace();
        }
    }

}
