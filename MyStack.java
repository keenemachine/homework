package asolovyov.stack;

import java.util.EmptyStackException;

public class MyStack<T> {

    T[] elements;
    int index = -1;
    int size;
    int defaultCapacity = 10;
    int capacity;

    public MyStack() {
        elements = (T[]) new Object[defaultCapacity];
        capacity = defaultCapacity;
        size = 0;
    }

    //класть
    public void push(T element) throws StackOverflowError {
        if (size == capacity) {
            throw new StackOverflowError("Стек заполнен");
        } else {
            elements[size++] = element;
        }
    }

    //брать
    public T pop() throws EmptyStackException {
        if (size == 0) {
            throw new EmptyStackException();
        } else {
            T result = elements[size-1];
            size--;
            return result;
        }
    }

}

/** T - произвольный параметр типа данных
 * Дженерик - конструкция (шаблон) для упрощения работы с разными типами данных
 */